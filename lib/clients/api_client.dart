import 'dart:convert';

import 'package:http/http.dart' as http;

class ApiClient {
  final String apiAddress = 'api.unsplash.com';

  final String accessKey = '5NJLuL5mKV_zeHz6RiAfCqAsq4OY5j4gJfpYbMsMN44';

  _validateCodes(int statusCode) {
    print('Api client: Response status code $statusCode.');
  }

  Future photosList(int page) async {
    assert(page != null);
    final url = Uri.https(apiAddress, '/photos', {
      'client_id': accessKey,
      'page': page.toString(),
    });
    final response = await http.get(url);

    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      _validateCodes(response.statusCode);
    }
  }
}
