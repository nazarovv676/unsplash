import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../components/photo_tile.dart';
import '../../repos/api_repo.dart';
import 'bloc/photos_bloc.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      //provide photos business logic to context
      create: (context) => PhotosBloc(apiRepo: context.read<ApiRepo>()),
      child: Scaffold(
        backgroundColor: Colors.blueGrey[300],
        appBar: AppBar(
          title: Text('Unsplash gallary'),
        ),
        body: Center(
          child: BlocBuilder<PhotosBloc, PhotosState>(
            builder: (context, state) {
              if (state is PhotosInitial) {
                //if programm started
                context.read<PhotosBloc>().add(PhotosRequested());
                return CircularProgressIndicator();
              } else if (state is PhotosInProgress) {
                return CircularProgressIndicator();
              } else if (state is PhotosFailure) {
                return Text(state.message);
              } else if (state is PhotosSuccess) {
                return SingleChildScrollView(
                  child: Column(
                    children: state.photos
                        .map(
                          //convert state data to widget
                          (e) => Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 5.0,
                              vertical: 2.5,
                            ),
                            child: PhotoTile(photo: e),
                          ),
                        )
                        .toList(),
                  ),
                );
              }
              return Text('Unknown photos bloc state $state');
            },
          ),
        ),
      ),
    );
  }
}
