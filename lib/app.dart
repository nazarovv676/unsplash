import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:unsplash/clients/api_client.dart';

import 'pages/home/home_page.dart';
import 'repos/api_repo.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.blue[100],
      ),
      home: RepositoryProvider(
        create: (context) => ApiRepo(apiClient: ApiClient()),
        child: HomePage(),
      ),
    );
  }
}
