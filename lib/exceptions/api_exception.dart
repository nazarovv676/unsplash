import 'package:flutter/widgets.dart';

class ApiException {
  final String message;

  ApiException({required this.message}) : assert(message != null);
}
