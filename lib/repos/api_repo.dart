import '../models/photo_model.dart';
import '../clients/api_client.dart';

///Works with unsplash API client
class ApiRepo {
  ///API client
  final ApiClient apiClient;

  ApiRepo({required this.apiClient});

  Future<List<Photo>> photos({int page = 1}) async {
    final List response = await apiClient.photosList(page);
    final photo = response
        .map((e) => Photo(
              id: e['id'],
              small: e['urls']['small'],
              regular: e['urls']['regular'],
              full: e['urls']['full'],
              userName: e['user']['name'],
              likes: e['likes'],
              blurHash: e['blur_hash'],
            ))
        .toList();
    return photo;
  }
}
